//
//  ViewController.m
//  DemoLogger
//
//  Created by Snehal Mehta on 11/13/13.
//  Copyright (c) 2013 Testing. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    int i = 0;
    for(i = 0 ; i < 1000 ; i++)
    {
        FLog(@"Current value %d ",i);
    }
}

- (IBAction)showLogs:(id)sender
{
    [FileLogger showLogs:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
